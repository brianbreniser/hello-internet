#!/bin/bash

yes | tkn t delete git-clone
yes | tkn t delete show-workspaces-working
yes | tkn p delete code-pipeline

oc create -f git-clone.yaml
oc create -f show-workspaces-working-t.yaml
oc create -f code-pipeline.yaml

sleep 1

oc create -f pipeline-runner.yaml

sleep 1

tkn p logs code-pipeline
