#!/bin/env bash
set -euxv # e for whole script failure if one command fails, -x for printing every command run, and -u to error on unset variables (echo $THING fails if $THING isn't set), v for verbose
set -o pipefail # fail if one part of a pipe fails (From manpage: If  set, the return value of a pipeline is the value of the last (rightmost) command to exit with a non-zero status, or zero if all commands in the pipeline exit successfully.  This option is disabled by default.)

pkill -f "java -jar target/hello_world_quarkus-1.0.0-SNAPSHOT-runner.jar"
