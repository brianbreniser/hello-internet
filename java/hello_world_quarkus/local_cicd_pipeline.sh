#!/bin/bash
set -euxv # e for whole script failure if one command fails, -x for printing every command run, and -u to error on unset variables (echo $THING fails if $THING isn't set), v for verbose
set -o pipefail # fail if one part of a pipe fails (From manpage: If  set, the return value of a pipeline is the value of the last (rightmost) command to exit with a non-zero status, or zero if all commands in the pipeline exit successfully.  This option is disabled by default.)

source ~/sonartoken.sh

# Stop a running container with this name
if [[ `podman ps | grep hwq | wc -l` -ge 1 ]]; then podman stop hwq; fi

# Wipe the named container, if it exists
if [[ `podman ps -a -f name=hwq | grep hwq | wc -l` -ge 1 ]]; then podman rm hwq; fi

# Just sets up our build files
podman build -t hello_world_quarkus -f Dockerfile.build .

# Background the container so it is running
podman run -it -d --name hwq localhost/hello_world_quarkus bash

# From this point on, the container is running under the 'hwq' name, so we can run commands against it

podman exec -it hwq ./cicd/step_1_compile.sh
podman exec -it hwq ./cicd/step_2_unit_test.sh
podman exec -it hwq ./cicd/step_3_sonarqube_scan.sh "$SONAR_TOKEN"
# podman exec -it hwq ./cicd/step_4_package.sh
# podman exec -it hwq ./cicd/step_5_build_runtime_container.sh
