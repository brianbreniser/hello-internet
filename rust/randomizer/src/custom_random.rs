extern crate rand;
use rand::random;
use self::rand::Rng;

pub fn rand_i64_range(min: i64, max: i64) -> i64 {
    rand::thread_rng().gen_range(min, max+1)
}

pub fn rand_i64() -> i64 {
    random::<i64>()
}

fn our_name_list() -> Vec<&'static str> {
    return vec!["Brian", "Erin", "Sara", "Miro", "Xenia", "Bria", "Larry", "Chris", "Steve"];
}

pub fn rand_name(name_list: Option<Vec<&'static str>>) -> String {
    match name_list {
        Some(name_list) => {
            let max = name_list.len() as i64;
            let random_index = rand_i64_range(1, max-1);
            String::from(name_list[random_index as usize])
        },
        None => {
            let name_list = our_name_list();
            let max = name_list.len() as i64;
            let random_index = rand_i64_range(1, max-1);
            String::from(name_list[random_index as usize])
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn rand_num_returns_exact_number_when_limits_set() {
        assert_eq!(rand_i64_range(10, 10), 10);
        assert_eq!(rand_i64_range(11, 11), 11);
        assert_eq!(rand_i64_range(0, 0), 0);
    }

    #[test]
    fn rand_num_returns_well_on_0_100() {
        for _ in 0..100 {
            let a = rand_i64_range(1, 100);
            println!("{}", a);
            assert!(100 >= a && a >= 1);
        }
    }
}
