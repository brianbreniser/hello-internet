#![feature(proc_macro_hygiene, decl_macro)]
#[macro_use] extern crate rocket;

mod custom_random;
use custom_random::{rand_i64, rand_i64_range};

#[catch(404)]
pub fn not_found() -> &'static str {
    "Oooops, 404, nothing here to see, folks"
}

#[get("/")]
fn index() -> &'static str {
    "Hello, world!"
}

#[get("/random/randint")]
fn randint_endpoint() -> String {
    format!("{}", rand_i64())
}

#[get("/random/randint/<min>/<max>")]
fn randint_endpoint_range(min: i64, max: i64) -> String {
    let result: i64 = rand_i64_range(min, max);
    format!("{}", result)
}

fn main() {
    rocket::ignite()
        .register(
            catchers![
                not_found
        ])
        .mount(
            "/",
            routes![
                randint_endpoint,
                randint_endpoint_range,
                index])
        .launch();
}
